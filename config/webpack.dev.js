const HtmlWebpackPlugin = require('html-webpack-plugin');
const { merge } = require('webpack-merge');

const path = require('path');

const appConfig = require('./webpack.app');

/** @type {import('webpack').Configuration} */
const devConfig = {
  mode: 'development',
  devtool: 'eval-source-map',
  devServer: {
    // server: 'https',
    // server: {
    //   type: 'https',
    //   options: {
    //     ca: './certs/server.pem',
    //     pfx: './certs/server.pfx',
    //     key: './certs/server.key',
    //     cert: './certs/server.crt',
    //     passphrase: '',
    //     requestCert: true
    //   }
    // },
    devMiddleware: {
      writeToDisk: true
    }
  },
  plugins: [
    new HtmlWebpackPlugin({
      publicPath: 'auto',
      template: path.resolve(__dirname, '../src/index.html')
    })
  ]
};

module.exports = merge(appConfig, devConfig);
