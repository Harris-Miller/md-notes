module.exports = {
  ignore: ['**/*.test.*', '**/__test__', '**/__mocks__'],
  presets: [
    [
      '@babel/preset-typescript',
      {
        isTSX: true,
        allExtensions: true
      }
    ],
    ['@babel/preset-react', { runtime: 'automatic' }],
    [
      '@babel/preset-env',
      {
        useBuiltIns: 'usage',
        corejs: '3.19',
        targets: 'defaults' // for now
      }
    ]
  ],
  plugins: [
    '@babel/plugin-syntax-dynamic-import',
    '@babel/plugin-transform-runtime',
    '@babel/plugin-proposal-class-properties'
  ]
};
