const HtmlWebpackPlugin = require('html-webpack-plugin');
const { merge } = require('webpack-merge');

const path = require('path');

const appConfig = require('./webpack.app');

/** @type {import('webpack').Configuration} */
const prodConfig = {
  mode: 'production',
  // output: {
  //   publicPath: './'
  // },
  plugins: [
    new HtmlWebpackPlugin({
      publicPath: 'auto',
      template: path.resolve(__dirname, '../src/index.html'),
      filename: './index.html',
      minify: {
        html5: true,
        collapseWhitespace: true,
        removeComments: true,
        removeEmptyAttributes: true
      }
    })
  ]
};

module.exports = merge(appConfig, prodConfig);
