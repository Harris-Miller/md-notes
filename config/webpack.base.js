const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const { DefinePlugin } = require('webpack');

const path = require('path');

const babelConfig = require('./babel.config');

const env = [
  'FB_API_KEY',
  'FB_AUTH_DOMAIN',
  'FB_PROJECT_ID',
  'FB_STORAGE_BUCKET',
  'FB_MESSAGING_SENDER_ID',
  'FB_APP_ID',
  'MEASUREMENT_ID'
].reduce((acc, key) => {
  acc[key] = JSON.stringify(process.env[key]);
  return acc;
}, {});

/** @type {import('webpack').Configuration} */
module.exports = {
  optimization: {
    splitChunks: {
      chunks: 'all'
    }
  },
  resolve: {
    extensions: ['.js', '.jsx', '.ts', '.tsx', '.json']
  },
  plugins: [
    // ensure paths are cased correctly even on windows machines
    new CaseSensitivePathsPlugin(),
    // run ts type checker in a separate process for faster builds
    new ForkTsCheckerWebpackPlugin({
      typescript: {
        configFile: path.resolve(__dirname, './tsconfig.json')
      }
    }),
    // replace modules without refreshing the page
    new DefinePlugin({
      'process.env': env
    })
  ],
  module: {
    rules: [
      // transpile js, jsx, ts, and tsx with babel
      {
        test: [/\.[jt]sx?$/],
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              cacheDirectory: true,
              ...babelConfig
            }
          }
        ]
      },
      // inject css into index.html
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
      // handle images
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: ['file-loader']
      },
      // handle fonts
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: ['file-loader']
      }
    ]
  }
};
