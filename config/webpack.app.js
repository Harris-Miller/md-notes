const { merge } = require('webpack-merge');

const path = require('path');

const baseConfig = require('./webpack.base');

/** @type {import('webpack').Configuration} */
const appConfig = {
  entry: {
    app: path.resolve(__dirname, '../src/index.tsx')
  },
  output: {
    path: path.resolve(__dirname, '../dist'),
    filename: '[name].bundle.js',
    chunkFilename: '[name].chunk.js',
    clean: true
  },
  resolve: {
    alias: {
      '@mui/styled-engine': '@mui/styled-engine-sc',
      lodash: 'lodash-es'
    }
  }
};

module.exports = merge(baseConfig, appConfig);
