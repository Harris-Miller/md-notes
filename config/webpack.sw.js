const { merge } = require('webpack-merge');

const path = require('path');

const baseConfig = require('./webpack.base');

/** @type {import('webpack').Configuration} */
const prodConfig = {
  mode: 'production',
  target: 'webworker',
  devtool: 'eval-source-map', // uncomment when you need source maps in development
  entry: {
    sw: path.resolve(__dirname, '../sw/index.ts')
  },
  output: {
    path: path.resolve(__dirname, '../public')
  },
  resolve: {
    alias: {
      lodash: 'lodash-es'
    }
  }
};

module.exports = merge(baseConfig, prodConfig);
