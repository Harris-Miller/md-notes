import { collection, CollectionReference, doc, getDocs, query, updateDoc, where } from 'firebase/firestore';
import { getDownloadURL, getStorage, ref, uploadBytes } from 'firebase/storage';

import { INote } from '../store/notes/notes.state';
import { IUserConfig } from '../store/userConfig/userConfig.state';

import { firestore } from './app';

const getCollectionForUser = <T>(collectionRef: CollectionReference<T>, uid: string) =>
  getDocs(query(collectionRef, where('uid', '==', uid)));

// notes
const NOTES_COLLECTION = 'notes';
const notesCollection = collection(firestore, NOTES_COLLECTION) as CollectionReference<INote>;
export const getNotes = async (uid: string) => getCollectionForUser(notesCollection, uid);
export const updateNote = async (id: string, note: Partial<INote>) =>
  updateDoc(doc(firestore, NOTES_COLLECTION, id), note);

// userConfig
const USER_CONFIG_COLLECTION = 'userConfig';
const userConfigCollection = collection(firestore, USER_CONFIG_COLLECTION) as CollectionReference<IUserConfig>;
export const getUserConfig = async (uid: string) => getCollectionForUser(userConfigCollection, uid);
export const updateUserConfig = async (id: string, userConfig: Partial<IUserConfig>) =>
  updateDoc(doc(firestore, USER_CONFIG_COLLECTION, id), userConfig);

// storage
const storage = getStorage();
/**
 * Uploads a file and returns the downloadURL
 */
export const uploadFile = async (file: File) => {
  const uploadResults = await uploadBytes(ref(storage, file.name), file);
  return getDownloadURL(uploadResults.ref);
};
