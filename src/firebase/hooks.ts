import { AuthError, User } from 'firebase/auth';
import { useAuthState } from 'react-firebase-hooks/auth';

import { auth } from './app';

// typing sucks for this
const useAppAuthState = () => useAuthState(auth) as [User, boolean, AuthError];
export { useAppAuthState as useAuthState };
