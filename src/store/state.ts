import { INotesState } from './notes/notes.state';
import { IUserConfigState } from './userConfig/userConfig.state';

interface IStoreState {
  notes: INotesState;
  userConfig: IUserConfigState;
}

export default IStoreState;
