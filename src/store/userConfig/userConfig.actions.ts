import { IUserConfig } from './userConfig.state';

export const FETCH_USER_CONFIG = 'FETCH_USER_CONFIG';
export const UPDATE_USER_CONFIG = 'UPDATE_USER_CONFIG';
export const SET_USER_CONFIG = 'SET_USER_CONFIG';
export const TOGGLE_MENU_DRAWER = 'TOGGLE_MENU_DRAWER';

export const fetchUserConfig = (uid: string) => ({
  type: FETCH_USER_CONFIG,
  uid
});

export type TFetchUserConfig = ReturnType<typeof fetchUserConfig>;

export const updateUserConfig = (partialUpdate: Partial<IUserConfig>) => ({
  type: UPDATE_USER_CONFIG,
  partialUpdate
});

export type TUpdateUserConfig = ReturnType<typeof updateUserConfig>;

export const setUserConfig = (id: string, partialUpdate: Partial<IUserConfig>) => ({
  type: SET_USER_CONFIG,
  id,
  partialUpdate
});

export type TSetUserConfig = ReturnType<typeof setUserConfig>;

export const toggleMenuDrawer = () => ({
  type: TOGGLE_MENU_DRAWER
});

export type TToggleMenuDrawer = ReturnType<typeof toggleMenuDrawer>;

export type TUserConfigActions = TFetchUserConfig | TUpdateUserConfig | TSetUserConfig | TToggleMenuDrawer;
