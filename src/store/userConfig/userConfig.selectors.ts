import IStoreState from '../state';

export const selectUserConfigId = (state: IStoreState) => state.userConfig.id;

export const selectPaletteMode = (state: IStoreState) => state.userConfig.userConfig.paletteMode;

export const selectIsMenuDrawerOpen = (state: IStoreState) => state.userConfig.userConfig.isMenuDrawerOpen;
