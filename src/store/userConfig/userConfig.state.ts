export type TPaletteMode = 'light' | 'dark' | 'auto';

export interface IUserConfig {
  paletteMode: TPaletteMode;
  isMenuDrawerOpen: boolean;
}

export interface IUserConfigState {
  id: string;
  userConfig: IUserConfig;
}

export const defaultUserConfigState: IUserConfigState = {
  id: '',
  userConfig: {
    paletteMode: 'light',
    isMenuDrawerOpen: false
  }
};
