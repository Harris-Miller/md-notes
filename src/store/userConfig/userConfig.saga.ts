import { QuerySnapshot } from 'firebase/firestore';
import { call, put, select, takeEvery, takeLatest } from 'redux-saga/effects';

import { getUserConfig as getUserConfigFB, updateUserConfig as updateUserConfigFB } from '../../firebase';

import {
  FETCH_USER_CONFIG,
  setUserConfig,
  TFetchUserConfig,
  TOGGLE_MENU_DRAWER,
  TUpdateUserConfig,
  UPDATE_USER_CONFIG,
  updateUserConfig as updateUserConfigAction
} from './userConfig.actions';
import { selectUserConfigId } from './userConfig.selectors';
import { IUserConfig } from './userConfig.state';

function* fetchUserConfig({ uid }: TFetchUserConfig) {
  const results: QuerySnapshot<IUserConfig> = yield call(getUserConfigFB, uid);
  const userConfig = results.docs[0]; // there will only ever be one per user
  yield put(setUserConfig(userConfig.id, userConfig.data()));
}

function* updateUserConfig({ partialUpdate }: TUpdateUserConfig) {
  const id: string = yield select(selectUserConfigId);
  yield put(setUserConfig(id, partialUpdate));
  yield call(updateUserConfigFB, id, partialUpdate);
}

function* toggleMenuDrawer() {
  const currentState = select();
  yield put(updateUserConfigAction({ isMenuDrawerOpen: !currentState }));
}

export function* userConfigSaga() {
  yield takeLatest(FETCH_USER_CONFIG, fetchUserConfig);
  yield takeEvery(UPDATE_USER_CONFIG, updateUserConfig);
  yield takeEvery(TOGGLE_MENU_DRAWER, toggleMenuDrawer);
}
