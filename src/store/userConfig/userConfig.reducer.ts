import { SET_USER_CONFIG, TSetUserConfig, TUserConfigActions } from './userConfig.actions';
import { defaultUserConfigState, IUserConfigState } from './userConfig.state';

export const userConfigReducer = (
  state: IUserConfigState = defaultUserConfigState,
  action: TUserConfigActions
): IUserConfigState => {
  switch (action.type) {
    case SET_USER_CONFIG: {
      const { id, partialUpdate } = action as TSetUserConfig;
      return { id, userConfig: { ...state.userConfig, ...partialUpdate } };
    }

    default:
      return state;
  }
};
