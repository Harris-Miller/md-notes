import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import { notesReducer } from './notes';
import { userConfigReducer } from './userConfig';

export const persistConfig = {
  key: 'root',
  storage
};

const reducers = combineReducers({
  notes: notesReducer,
  userConfig: persistReducer(persistConfig, userConfigReducer)
});

export default reducers;
