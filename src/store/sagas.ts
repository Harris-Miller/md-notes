import { all, call, spawn } from 'redux-saga/effects';

import { notesSaga } from './notes';
import { userConfigSaga } from './userConfig';

function* rootSaga() {
  const sagas = [notesSaga, userConfigSaga];

  yield all(
    sagas.map(saga =>
      spawn(function* runSaga() {
        while (true) {
          try {
            yield call(saga);
            break;
          } catch (e) {
            // eslint-disable-next-line no-console
            console.error(e);
          }
        }
      })
    )
  );
}

export default rootSaga;
