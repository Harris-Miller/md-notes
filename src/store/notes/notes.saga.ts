import { QuerySnapshot } from 'firebase/firestore';
import { call, put, takeLatest } from 'redux-saga/effects';

import { auth, getNotes, updateNote } from '../../firebase';

import {
  FETCH_NOTES_COLLECTION,
  resetNotes,
  SET_NOTE_TEXT,
  SET_NOTE_TITLE,
  setIsLoading,
  TFetchNotesCollection,
  TSetNoteText,
  TSetNoteTitle
} from './notes.actions';
import { INote } from './notes.state';

function* fetchNotesCollection({ uid }: TFetchNotesCollection) {
  yield put(setIsLoading(true));
  const results: QuerySnapshot<INote> = yield call(getNotes, uid);
  const notes = results.docs.map(result => {
    const { text, title } = result.data();
    return { id: result.id, text, title, uid };
  });
  yield put(resetNotes(notes));
}

// TODO: extract duplicate code
function* setNoteText({ id, text }: TSetNoteText) {
  if (auth.currentUser) {
    try {
      yield call(updateNote, id, { text } as INote);
    } catch (e) {
      // TODO
      // eslint-disable-next-line no-console
      console.error(e);
    }
  }
}

function* setNoteTitle({ id, title }: TSetNoteTitle) {
  if (auth.currentUser) {
    try {
      yield call(updateNote, id, { title } as INote);
    } catch (e) {
      // TODO
      // eslint-disable-next-line no-console
      console.error(e);
    }
  }
}

export function* notesSaga() {
  yield takeLatest(SET_NOTE_TEXT, setNoteText);
  yield takeLatest(SET_NOTE_TITLE, setNoteTitle);
  yield takeLatest(FETCH_NOTES_COLLECTION, fetchNotesCollection);
}
