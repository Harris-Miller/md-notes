import memoize from 'proxy-memoize';

import IStoreState from '../state';

export const selectIsLoading = (state: IStoreState) => state.notes.isLoading;

export const selectNotesMeta = memoize((state: IStoreState) =>
  Object.entries(state.notes.notes).map(([id, note]) => ({ id, title: note.title }))
);

export const selectSelectedNoteId = (state: IStoreState) => state.notes.selectedNote;

const selectSelectedNote = (state: IStoreState) => state.notes.notes[state.notes.selectedNote];

export const selectSelectedNoteText = (state: IStoreState) => selectSelectedNote(state)?.text ?? '';

export const selectSelectedNoteTitle = (state: IStoreState) => selectSelectedNote(state)?.title ?? '';
