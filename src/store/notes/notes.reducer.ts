import {
  CLEAR_NOTES,
  RESET_NOTES,
  SET_IS_LOADING,
  SET_NOTE_TEXT,
  SET_NOTE_TITLE,
  SET_SELECTED_NOTE,
  TNotesActions,
  TResetNotes,
  TSetNoteText,
  TSetNoteTitle,
  TSetSelectedNote
} from './notes.actions';
import { INotesState } from './notes.state';

export const defaultNoteState: INotesState = {
  isLoading: false,
  selectedNote: '',
  notes: {}
};

export const notesReducer = (state: INotesState = defaultNoteState, action: TNotesActions): INotesState => {
  switch (action.type) {
    case CLEAR_NOTES: {
      return defaultNoteState;
    }
    case SET_IS_LOADING: {
      return { ...state, isLoading: true };
    }
    case RESET_NOTES: {
      const { notes } = action as TResetNotes;
      const transformed = notes.reduce((acc, { id, ...note }) => {
        acc[id] = note;
        return acc;
      }, {} as INotesState['notes']);

      return {
        ...state,
        isLoading: false,
        selectedNote: Object.keys(transformed)[0] ?? '',
        notes: transformed
      };
    }
    case SET_SELECTED_NOTE: {
      const { id } = action as TSetSelectedNote;
      return {
        ...state,
        selectedNote: id
      };
    }
    case SET_NOTE_TEXT: {
      const { id, text } = action as TSetNoteText;
      return {
        ...state,
        notes: {
          ...state.notes,
          [id]: { ...state.notes[id], text }
        }
      };
    }
    case SET_NOTE_TITLE: {
      const { id, title } = action as TSetNoteTitle;
      return {
        ...state,
        notes: {
          ...state.notes,
          [id]: { ...state.notes[id], title }
        }
      };
    }
    default:
      return state;
  }
};
