export interface INote {
  uid: string;
  text: string;
  title: string;
}

export interface INotesState {
  isLoading: boolean;
  selectedNote: string;
  notes: {
    [id: string]: INote;
  };
}
