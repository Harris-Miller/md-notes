import { INote } from './notes.state';

export const CLEAR_NOTES = 'CLEAR_NOTES';
export const SET_IS_LOADING = 'SET_IS_LOADING';
export const FETCH_NOTES_COLLECTION = 'FETCH_NOTES_COLLECTION';
export const RESET_NOTES = 'RESET_NOTES';
export const SET_SELECTED_NOTE = 'SET_SELECTED_NOTE';
export const SET_NOTE_TEXT = 'SET_NOTE_TEXT';
export const SET_NOTE_TITLE = 'SET_NOTE_TITLE';

export const clearNotes = () => ({
  type: CLEAR_NOTES
});

export type TClearNotes = ReturnType<typeof clearNotes>;

export const setIsLoading = (isLoading: boolean) => ({
  type: SET_IS_LOADING,
  isLoading
});

export type TSetIsLoding = ReturnType<typeof setIsLoading>;

export const fetchNotesCollection = (uid: string) => ({
  type: FETCH_NOTES_COLLECTION,
  uid
});

export type TFetchNotesCollection = ReturnType<typeof fetchNotesCollection>;

export const resetNotes = (notes: (INote & { id: string })[]) => ({
  type: RESET_NOTES,
  notes
});

export type TResetNotes = ReturnType<typeof resetNotes>;

export const setSelectedNote = (id: string) => ({
  type: SET_SELECTED_NOTE,
  id
});

export type TSetSelectedNote = ReturnType<typeof setSelectedNote>;

export const setNoteText = (id: string, text: string) => ({
  type: SET_NOTE_TEXT,
  id,
  text
});

export type TSetNoteText = ReturnType<typeof setNoteText>;

export const setNoteTitle = (id: string, title: string) => ({
  type: SET_NOTE_TITLE,
  id,
  title
});

export type TSetNoteTitle = ReturnType<typeof setNoteTitle>;

export type TNotesActions =
  | TClearNotes
  | TSetIsLoding
  | TFetchNotesCollection
  | TResetNotes
  | TSetSelectedNote
  | TSetNoteText
  | TSetNoteTitle;
