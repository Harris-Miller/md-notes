import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';
import { applyMiddleware, createStore, Dispatch } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { persistStore } from 'redux-persist';
import createSagaMiddleware from 'redux-saga';

import { TNotesActions } from './notes';
import reducers from './reducers';
import sagas from './sagas';
import IStoreState from './state';
import { TUserConfigActions } from './userConfig';

// @ts-expect-error
// eslint-disable-next-line no-var
declare var module = globalThis.module as NodeModule & {
  hot: { accept: (path: string, callback: () => void) => void };
};

export type TStoreActions = TNotesActions | TUserConfigActions;

const sagaMiddleware = createSagaMiddleware();

const composedEnhancers = composeWithDevTools(applyMiddleware(sagaMiddleware));
const store = createStore(reducers, undefined, composedEnhancers);

let sagaTask = sagaMiddleware.run(sagas);

if (module.hot) {
  // Enable Webpack hot module replacement for reducers
  // TODO: test out if these "accepts" work for what them import as well
  module.hot.accept('./reducers', async () => {
    const nextReducers = (await import('./reducers')).default;
    store.replaceReducer(nextReducers);
  });
  module.hot.accept('./sagas', async () => {
    const nextSagas = (await import('./sagas')).default;
    sagaTask.cancel();
    sagaTask.toPromise().then(() => {
      sagaTask = sagaMiddleware.run(nextSagas);
    });
  });
}

export default store;

export const persistor = persistStore(store);

const customUseSelector: TypedUseSelectorHook<IStoreState> = useSelector;
const customUseDispatch: () => Dispatch<TStoreActions> = useDispatch;
export { customUseSelector as useSelector };
export { customUseDispatch as useDispatch };
