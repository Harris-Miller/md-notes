import Box from '@mui/material/Box';
import { FC } from 'react';

import AppHeader from './header/AppHeader';
import MDEditor from './mdEditor';
import MenuDrawer from './menuDrawer';
import Splash from './splash';
import ApperanceWrapper from './wrappers/ApperanceWrapper';
import StoreWrapper from './wrappers/StoreWrapper';
import UiWrapper from './wrappers/UiWrapper';

const App: FC = () => (
  <StoreWrapper>
    <ApperanceWrapper>
      <UiWrapper>
        <Box sx={{ display: 'flex', height: '100%' }}>
          <AppHeader />
          <Splash>
            <MenuDrawer />
            <MDEditor />
          </Splash>
        </Box>
      </UiWrapper>
    </ApperanceWrapper>
  </StoreWrapper>
);

export default App;
