import Box from '@mui/material/Box';
import CircularProgress from '@mui/material/CircularProgress';
import { FC } from 'react';

import { useSelector } from '../../store';
import { selectIsLoading } from '../../store/notes';

const Splash: FC = ({ children }) => {
  const isLoading = useSelector(selectIsLoading);

  if (isLoading) {
    return (
      <Box
        sx={{
          display: 'flex',
          alignSelf: 'stretch',
          alignItems: 'center',
          justifyContent: 'center',
          width: '100%'
        }}
      >
        <CircularProgress />
      </Box>
    );
  }

  return <>{children}</>;
};

export default Splash;
