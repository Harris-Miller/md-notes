import { FC, useEffect, useState } from 'react';

import { useDispatch, useSelector } from '../../store';
import { selectPaletteMode } from '../../store/userConfig';
import { AppearanceProvider, TMode } from '../context/appearance';

const AppearanceWrapper: FC = ({ children }) => {
  const palleteMode = useSelector(selectPaletteMode);
  const [mode, setMode] = useState<TMode>(palleteMode === 'auto' ? 'light' : palleteMode);
  const dispatch = useDispatch();

  useEffect(() => {
    // declare listener
    let listener: (e: MediaQueryListEvent) => void;

    if (palleteMode === 'auto') {
      // set if auto
      listener = (e: MediaQueryListEvent) => {
        setMode(e.matches ? 'dark' : 'light');
      };

      // do an initial set
      setMode(window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches ? 'dark' : 'light');

      // attach listener to event for changed
      window.matchMedia('(prefers-color-scheme: dark)').addEventListener('change', listener);
    } else {
      // else just set the mode directly
      setMode(palleteMode);
    }

    return () => {
      // remove listener if we set it
      if (listener) {
        window.matchMedia('(prefers-color-scheme: dark)').removeEventListener('change', listener);
      }
    };
  }, [dispatch, palleteMode]);

  return <AppearanceProvider value={mode}>{children}</AppearanceProvider>;
};

export default AppearanceWrapper;
