import CssBaseline from '@mui/material/CssBaseline';
import { createTheme, Theme, ThemeProvider } from '@mui/material/styles';
import { FC } from 'react';

import { TMode, useAppearance } from '../context/appearance';

// @ts-expect-error - for initial nulls
const themes: { light: Theme; dark: Theme } = { light: null, dark: null };

const getTheme = (mode: TMode) => {
  if (!themes[mode]) {
    themes[mode] = createTheme({
      palette: {
        mode
      }
    });
  }

  return themes[mode];
};

const Wrapper: FC = ({ children }) => {
  const mode = useAppearance();
  const theme = getTheme(mode);

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      {children}
    </ThemeProvider>
  );
};

export default Wrapper;
