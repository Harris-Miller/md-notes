import { createContext, useContext } from 'react';

export type TMode = 'light' | 'dark';

const appearanceContext = createContext<TMode>('light');

const { Provider } = appearanceContext;

export { Provider as AppearanceProvider };

export const useAppearance = () => useContext(appearanceContext);
