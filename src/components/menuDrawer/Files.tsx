import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import ListSubheader from '@mui/material/ListSubheader';
import { FC } from 'react';

import { useDispatch, useSelector } from '../../store';
import { selectNotesMeta, selectSelectedNoteId, setSelectedNote } from '../../store/notes';

const Files: FC = () => {
  const selectedNoteId = useSelector(selectSelectedNoteId);
  const notesMeta = useSelector(selectNotesMeta);
  const dispatch = useDispatch();

  const setSelected = (id: string) => () => {
    dispatch(setSelectedNote(id));
  };

  return (
    <List component="div">
      <ListSubheader>Notes</ListSubheader>
      <List component="div">
        {notesMeta.map(({ id, title }) => (
          <ListItemButton key={id} onClick={setSelected(id)} selected={selectedNoteId === id} sx={{ pl: 4 }}>
            <ListItemText primary={title} />
          </ListItemButton>
        ))}
      </List>
    </List>
  );
};

export default Files;
