import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListSubheader from '@mui/material/ListSubheader';
import { styled, Theme } from '@mui/material/styles';
import { lexer, Tokens } from 'marked';
import { useEffect, useState, VFC } from 'react';
import { Dictionary } from 'ts-essentials';

import { useSelector } from '../../store';
import { selectSelectedNoteText } from '../../store/notes';

const PREFIX = 'Indented';

const classes = {
  1: `${PREFIX}-one`,
  2: `${PREFIX}-two`,
  3: `${PREFIX}-three`,
  4: `${PREFIX}-four`
} as Dictionary<string, number>;

const Indented = styled(ListItem)(({ theme }: { theme: Theme }) => ({
  [`&.${classes[1]}`]: {
    fontSize: theme.typography.fontSize * 1.25,
    fontWeight: 'bold',
    paddingLeft: theme.spacing(5)
  },
  [`&.${classes[2]}`]: {
    paddingLeft: theme.spacing(7)
  },
  [`&.${classes[3]}`]: {
    paddingLeft: theme.spacing(9)
  },
  [`&.${classes[4]}`]: {
    paddingLeft: theme.spacing(11)
  }
}));

// TODO
const TableOfContents: VFC = () => {
  const text = useSelector(selectSelectedNoteText) ?? '';
  const [tokens, setTokens] = useState<Tokens.Heading[]>([]);

  useEffect(() => {
    setTokens(lexer(text).filter(token => token.type === 'heading') as Tokens.Heading[]);
  }, [text]);

  return (
    <List>
      <ListSubheader>Table Of Contents</ListSubheader>
      {tokens.map((token, i) => (
        // there is no way to make token unique
        // React says to use `i` as a last resort, see: https://reactjs.org/docs/lists-and-keys.html#keys
        // eslint-disable-next-line react/no-array-index-key
        <Indented className={classes[token.depth]} key={i}>
          {token.text}
        </Indented>
      ))}
    </List>
  );
};

export default TableOfContents;
