import Divider from '@mui/material/Divider';
import Drawer from '@mui/material/Drawer';
import Toolbar from '@mui/material/Toolbar';
import { FC } from 'react';

import { useDispatch, useSelector } from '../../store';
import { selectIsMenuDrawerOpen, toggleMenuDrawer } from '../../store/userConfig';

import Files from './Files';
import TableOfContents from './TableOfContents';

// got this value straight from mui docs for <Drawer>
const drawerWidth = 240;

const MenuDrawer: FC = () => {
  const isMenuDrawerOpen = useSelector(selectIsMenuDrawerOpen);
  const dispatch = useDispatch();

  const handleDrawerToggle = () => {
    dispatch(toggleMenuDrawer());
  };

  return (
    <Drawer
      container={window.document.body}
      onClose={handleDrawerToggle}
      open={isMenuDrawerOpen}
      sx={{
        width: drawerWidth,
        // todo: figure this out
        [`& .MuiDrawer-paper`]: { width: drawerWidth, boxSizing: 'border-box' }
      }}
      variant="permanent"
    >
      <Toolbar />
      <Divider />
      <Files />
      <Divider />
      <TableOfContents />
    </Drawer>
  );
};

export default MenuDrawer;
