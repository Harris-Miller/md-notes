import AccountCircle from '@mui/icons-material/AccountCircle';
import MuiAvatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import { styled, Theme } from '@mui/material/styles';
import { signInWithPopup, signOut } from 'firebase/auth';
import { MouseEventHandler, useEffect, useState, VFC } from 'react';

import { auth, googleAuthProvider, useAuthState } from '../../firebase';
import { useDispatch } from '../../store';
import { clearNotes, fetchNotesCollection, resetNotes } from '../../store/notes';

const Avatar = styled(MuiAvatar)(({ theme }: { theme: Theme }) => ({
  height: theme.spacing(4),
  width: theme.spacing(4)
}));

const Login: VFC = () => {
  const [user /* , loading, error */] = useAuthState();
  const dispatch = useDispatch();
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);

  const openMenu: MouseEventHandler<HTMLButtonElement> = event => {
    setAnchorEl(event.currentTarget);
  };

  const closeMenu = () => {
    setAnchorEl(null);
  };

  const handleLogin = async () => {
    await signInWithPopup(auth, googleAuthProvider);
  };

  const handleLogout = async () => {
    await signOut(auth);
    dispatch(clearNotes());
  };

  useEffect(() => {
    const foobar = async () => {
      if (user) {
        dispatch(fetchNotesCollection(user.uid));
      } else {
        // clear the store
        dispatch(resetNotes([]));
      }
    };
    foobar();
  }, [user, dispatch]);

  return (
    <>
      <IconButton color="inherit" onClick={openMenu}>
        {user?.photoURL ? <Avatar src={user.photoURL} /> : <AccountCircle />}
      </IconButton>
      <Menu anchorEl={anchorEl} onClose={closeMenu} open={open}>
        {user ? <MenuItem onClick={handleLogout}>Logout</MenuItem> : <MenuItem onClick={handleLogin}>Login</MenuItem>}
      </Menu>
    </>
  );
};

export default Login;
