import { styled, Theme } from '@mui/material/styles';
import { SxProps } from '@mui/system'; // eslint-disable-line import/no-extraneous-dependencies
import {
  ChangeEventHandler,
  ExoticComponent,
  FocusEventHandler,
  forwardRef,
  HTMLAttributes,
  InputHTMLAttributes,
  KeyboardEventHandler,
  MutableRefObject,
  RefAttributes,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';

type TStyledAttributes<TSpecificHTMLAttributes extends HTMLAttributes<TElement>, TElement> = RefAttributes<TElement> &
  TSpecificHTMLAttributes & { sx?: SxProps };

type TStyledComponent<TSpecificHTMLAttributes extends HTMLAttributes<TElement>, TElement> = ExoticComponent<
  TStyledAttributes<TSpecificHTMLAttributes, TElement>
>;

const StyledInput: TStyledComponent<InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> = styled('input')(
  ({ theme }: { theme: Theme }) => ({
    ...theme.typography.h6,
    background: 'none',
    border: 0,
    color: 'inherit',
    padding: 0,
    outline: 'none'
  })
);

interface IInlineInputProps {
  isEditing: boolean;
  onComplete: (value: string) => void;
  onEscape: (value: string) => void;
  sx?: SxProps;
  value: string;
}

const InlineInput = forwardRef<HTMLInputElement, IInlineInputProps>(
  ({ isEditing, onComplete: onSubmit, onEscape, value, sx }, passedRef) => {
    const [editedValue, setEditedValue] = useState(value);
    const internalRef = useRef<HTMLInputElement>() as MutableRefObject<HTMLInputElement>;

    const ref = useMemo(() => (passedRef as MutableRefObject<HTMLInputElement>) ?? internalRef, [passedRef]);

    useEffect(() => {
      (ref.current as HTMLInputElement).focus();
    });

    useEffect(() => {
      setEditedValue(value);
    }, [value]);

    const onKeyDown: KeyboardEventHandler<HTMLInputElement> = ({ key }) => {
      if (key === 'Enter') {
        onSubmit(editedValue);
      } else if (key === 'Escape') {
        onEscape(editedValue);
        setEditedValue(value);
      }
    };

    const onBlur: FocusEventHandler<HTMLInputElement> = () => {
      onSubmit(editedValue);
    };

    const onChange: ChangeEventHandler<HTMLInputElement> = ({ currentTarget: { value: nextValue } }) => {
      setEditedValue(nextValue);
    };

    const displayValue = isEditing ? editedValue : value;

    return (
      <StyledInput
        disabled={!isEditing}
        onBlur={onBlur}
        onChange={onChange}
        onKeyDown={onKeyDown}
        ref={ref}
        size={displayValue.length}
        sx={sx}
        type="text"
        value={displayValue}
      />
    );
  }
);

InlineInput.defaultProps = {
  sx: undefined
};

export default InlineInput;
