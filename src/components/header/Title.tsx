import EditIcon from '@mui/icons-material/Edit';
import Box from '@mui/material/Box';
import { useState, VFC } from 'react';

import { useDispatch, useSelector } from '../../store';
import { selectSelectedNoteId, selectSelectedNoteTitle, setNoteTitle } from '../../store/notes';

import InlineInput from './InlineInput';

const Title: VFC = () => {
  const noteId = useSelector(selectSelectedNoteId);
  const title = useSelector(selectSelectedNoteTitle);
  const dispatch = useDispatch();
  const [isEditing, setIsEditing] = useState(false);
  const [isTitleHovering, setIsTitleOvering] = useState(false);

  const showEditIcon = isTitleHovering && !isEditing;

  const onEditClick = () => {
    setIsEditing(true);
  };

  const onEscape = () => {
    setIsEditing(false);
  };

  const onSubmit = (value: string) => {
    setIsEditing(false);
    dispatch(setNoteTitle(noteId, value));
  };

  return (
    <Box
      onClick={onEditClick}
      onMouseEnter={() => setIsTitleOvering(true)}
      onMouseLeave={() => setIsTitleOvering(false)}
      sx={{
        alignItems: 'center',
        cursor: isEditing ? 'default' : 'pointer',
        display: 'flex'
      }}
    >
      <InlineInput
        isEditing={isEditing}
        onComplete={onSubmit}
        onEscape={onEscape}
        sx={{ cursor: isEditing ? 'text' : 'pointer' }}
        value={title}
      />
      <EditIcon
        fontSize="small"
        sx={{
          marginLeft: -2,
          opacity: 0.5,
          visibility: showEditIcon ? 'visible' : 'hidden'
        }}
      />
    </Box>
  );
};

export default Title;
