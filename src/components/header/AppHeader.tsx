import MoonIcon from '@mui/icons-material/Brightness3';
import ComboIcon from '@mui/icons-material/Brightness4';
import SunIcon from '@mui/icons-material/Brightness7';
import MenuIcon from '@mui/icons-material/Menu';
import { AppBar, Box, Button, ButtonGroup, IconButton, Menu, MenuItem, Toolbar, Typography } from '@mui/material';
import { Theme } from '@mui/material/styles';
import { FC, useState } from 'react';

import { useDispatch, useSelector } from '../../store';
import { selectPaletteMode, toggleMenuDrawer, TPaletteMode, updateUserConfig } from '../../store/userConfig';

import Login from './Login';
import Title from './Title';

const AppHeader: FC = () => {
  const paletteMode = useSelector(selectPaletteMode);
  const dispatch = useDispatch();
  const [appearanceAnchorEl, setAppearanceAnchorEl] = useState<null | HTMLElement>(null);

  const isApperanceMenuOpen = Boolean(appearanceAnchorEl);

  const handleApperanceMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setAppearanceAnchorEl(event.currentTarget);
  };

  const handleApperanceMenuClose = () => {
    setAppearanceAnchorEl(null);
  };

  const handleApperanceChange = (nextPaletteMode: TPaletteMode) => () => {
    dispatch(updateUserConfig({ paletteMode: nextPaletteMode }));
    handleApperanceMenuClose();
  };

  const onMenuIconClick = () => {
    dispatch(toggleMenuDrawer());
  };

  const renderMenu = (
    <Menu
      anchorEl={appearanceAnchorEl}
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'right'
      }}
      keepMounted
      onClose={handleApperanceMenuClose}
      open={isApperanceMenuOpen}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'right'
      }}
    >
      <MenuItem>
        <ButtonGroup>
          <Button
            onClick={handleApperanceChange('light')}
            startIcon={<SunIcon />}
            variant={paletteMode === 'light' ? 'contained' : 'outlined'}
          >
            Light
          </Button>
          <Button
            onClick={handleApperanceChange('auto')}
            startIcon={<ComboIcon />}
            variant={paletteMode === 'auto' ? 'contained' : 'outlined'}
          >
            Auto
          </Button>
          <Button
            onClick={handleApperanceChange('dark')}
            startIcon={<MoonIcon />}
            variant={paletteMode === 'dark' ? 'contained' : 'outlined'}
          >
            Dark
          </Button>
        </ButtonGroup>
      </MenuItem>
    </Menu>
  );

  const getAppearanceMenuIcon = () => {
    switch (paletteMode) {
      case 'light':
        return <SunIcon />;
      case 'dark':
        return <MoonIcon />;
      case 'auto':
      default:
        return <ComboIcon />;
    }
  };

  return (
    <>
      <AppBar position="fixed" sx={{ zIndex: (theme: Theme) => theme.zIndex.drawer + 1 }}>
        <Toolbar>
          <IconButton
            aria-label="menu"
            color="inherit"
            edge="start"
            onClick={onMenuIconClick}
            size="large"
            sx={{ mr: 2 }}
          >
            <MenuIcon />
          </IconButton>
          <Typography component="div" variant="h6">
            MD Notes
          </Typography>
          <Box sx={{ display: 'flex', justifyContent: 'center', flexGrow: 1 }}>
            <Title />
          </Box>
          <IconButton color="inherit" onClick={handleApperanceMenuOpen}>
            {getAppearanceMenuIcon()}
          </IconButton>
          <Login />
        </Toolbar>
      </AppBar>
      {renderMenu}
    </>
  );
};

export default AppHeader;
