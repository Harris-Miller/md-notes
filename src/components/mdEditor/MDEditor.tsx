import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import debounce from 'lodash/debounce';
import { useEffect, useMemo, useState, VFC } from 'react';
import RichMarkdownEditor from 'rich-markdown-editor';

import { uploadFile } from '../../firebase';
import store, { useDispatch, useSelector } from '../../store';
import { selectSelectedNoteId, selectSelectedNoteText, setNoteText } from '../../store/notes';
import { useAppearance } from '../context/appearance';

const MDEditor: VFC = () => {
  const mode = useAppearance();
  const dark = mode === 'dark';
  const [defaultValue, setDefaultValue] = useState('');
  const selectedNoteId = useSelector(selectSelectedNoteId);
  const dispatch = useDispatch();

  useEffect(() => {
    // only do this on change of selectedNodeId
    // RichMarkdownEditor doesn't work like a input#type=text where you have to control it
    // it has it's own internal state that updates past it's value prop
    // so we only want to set it when we switch notes
    // changes as you type are still persisted to the store
    // if there is no current note, just use empty string
    const text = selectSelectedNoteText(store.getState()) ?? '';
    setDefaultValue(text);
  }, [selectedNoteId]);

  const onChange = useMemo(
    () =>
      debounce((getNextValue: () => string) => {
        const nextValue = getNextValue();
        dispatch(setNoteText(selectedNoteId, nextValue));
      }, 300),
    [dispatch, selectedNoteId]
  );

  return (
    <Box component="main" sx={{ flexGrow: 1, marginX: 3 }}>
      {/* Toolbar is here to act as a spacer to push down the Editor so it's below the actual toolbar */}
      <Toolbar />
      <RichMarkdownEditor dark={dark} onChange={onChange} uploadImage={uploadFile} value={defaultValue} />
    </Box>
  );
};

export default MDEditor;
