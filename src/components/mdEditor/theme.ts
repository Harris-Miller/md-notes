import { Theme } from '@mui/material/styles';

// IColors, IBase, and ITheme is deconstructed from <RichMarkdownEditor>'s prop theme
// TODO: the hard coded colors need to be changed to come from muiTheme

interface IColors {
  almostBlack: string;
  lightBlack: string;
  almostWhite: string;
  white: string;
  white10: string;
  black: string;
  black10: string;
  primary: string;
  greyLight: string;
  grey: string;
  greyMid: string;
  greyDark: string;
}

interface IBase extends IColors {
  fontFamily: string;
  fontFamilyMono: string;
  fontWeight: number;
  zIndex: number;
  link: string;
  placeholder: string;
  textSecondary: string;
  textLight: string;
  textHighlight: string;
  textHighlightForeground: string;
  selected: string;
  codeComment: string;
  codePunctuation: string;
  codeNumber: string;
  codeProperty: string;
  codeTag: string;
  codeString: string;
  codeSelector: string;
  codeAttr: string;
  codeEntity: string;
  codeKeyword: string;
  codeFunction: string;
  codeStatement: string;
  codePlaceholder: string;
  codeInserted: string;
  codeImportant: string;
  blockToolbarBackground: string;
  blockToolbarTrigger: string;
  blockToolbarTriggerIcon: string;
  blockToolbarItem: string;
  blockToolbarIcon: undefined;
  blockToolbarIconSelected: string;
  blockToolbarText: string;
  blockToolbarTextSelected: string;
  blockToolbarSelectedBackground: string;
  blockToolbarHoverBackground: string;
  blockToolbarDivider: string;
  noticeInfoBackground: string;
  noticeInfoText: string;
  noticeTipBackground: string;
  noticeTipText: string;
  noticeWarningBackground: string;
  noticeWarningText: string;
}

interface ITheme extends IBase {
  background: string;
  text: string;
  code: string;
  cursor: string;
  divider: string;
  toolbarBackground: string;
  toolbarHoverBackground: string;
  toolbarInput: string;
  toolbarItem: string;
  tableDivider: string;
  tableSelected: string;
  tableSelectedBackground: string;
  quote: string;
  codeBackground: string;
  codeBorder: string;
  horizontalRule: string;
  imageErrorBackground: string;
  scrollbarBackground: string;
  scrollbarThumb: string;
}

const createColors = (muiTheme: Theme): IColors => ({
  almostBlack: muiTheme.palette.grey['900'],
  lightBlack: muiTheme.palette.grey['800'],
  almostWhite: muiTheme.palette.grey['50'],
  white: muiTheme.palette.common.white,
  white10: 'rgba(255, 255, 255, 0.1)', // keep
  black: muiTheme.palette.common.black,
  black10: 'rgba(0, 0, 0, 0.1)', // keep
  primary: muiTheme.palette.primary.main,
  greyLight: muiTheme.palette.grey['100'],
  grey: muiTheme.palette.grey['400'],
  greyMid: muiTheme.palette.grey['600'],
  greyDark: muiTheme.palette.grey['700']
});

const createBase = (muiTheme: Theme, colors: ReturnType<typeof createColors>): IBase => ({
  ...colors,
  fontFamily: muiTheme.typography.fontFamily as string,
  // TODO: set this from theme
  fontFamilyMono: "'SFMono-Regular',Consolas,'Liberation Mono', Menlo, Courier,monospace",
  fontWeight: muiTheme.typography.fontWeightRegular as number,
  zIndex: 100, // TODO: figure out
  link: colors.primary,
  placeholder: '#B1BECC',
  textSecondary: '#4E5C6E',
  textLight: colors.white,
  textHighlight: '#b3e7ff',
  textHighlightForeground: colors.black,
  selected: colors.primary,
  codeComment: '#6a737d',
  codePunctuation: '#5e6687',
  codeNumber: '#d73a49',
  codeProperty: '#c08b30',
  codeTag: '#3d8fd1',
  codeString: '#032f62',
  codeSelector: '#6679cc',
  codeAttr: '#c76b29',
  codeEntity: '#22a2c9',
  codeKeyword: '#d73a49',
  codeFunction: '#6f42c1',
  codeStatement: '#22a2c9',
  codePlaceholder: '#3d8fd1',
  codeInserted: '#202746',
  codeImportant: '#c94922',

  blockToolbarBackground: colors.white,
  blockToolbarTrigger: colors.greyMid,
  blockToolbarTriggerIcon: colors.white,
  blockToolbarItem: colors.almostBlack,
  blockToolbarIcon: undefined,
  blockToolbarIconSelected: colors.black,
  blockToolbarText: colors.almostBlack,
  blockToolbarTextSelected: colors.black,
  blockToolbarSelectedBackground: colors.greyMid,
  blockToolbarHoverBackground: colors.greyLight,
  blockToolbarDivider: colors.greyMid,

  noticeInfoBackground: '#F5BE31',
  noticeInfoText: colors.almostBlack,
  noticeTipBackground: '#9E5CF7',
  noticeTipText: colors.white,
  noticeWarningBackground: '#FF5C80',
  noticeWarningText: colors.white
});

export const createRichMarkupTheme = (muiTheme: Theme): ITheme => {
  const colors = createColors(muiTheme);

  return {
    ...createBase(muiTheme, colors),
    background: muiTheme.palette.background.default,
    text: muiTheme.palette.text.primary,
    code: muiTheme.palette.text.primary, // TODO
    cursor: muiTheme.palette.common.black,
    divider: muiTheme.palette.divider,

    toolbarBackground: colors.lightBlack,
    toolbarHoverBackground: colors.black,
    toolbarInput: colors.white10,
    toolbarItem: colors.white,

    tableDivider: colors.greyMid,
    tableSelected: colors.primary,
    tableSelectedBackground: '#E5F7FF',

    quote: colors.greyDark,
    codeBackground: colors.greyLight,
    codeBorder: colors.grey,
    horizontalRule: colors.greyMid,
    imageErrorBackground: colors.greyLight,

    scrollbarBackground: colors.greyLight,
    scrollbarThumb: colors.greyMid
  };
};
